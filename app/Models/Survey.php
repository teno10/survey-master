<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'survey';
    protected $primaryKey = 'id';
    public $timestamps = false;
}