<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public  function index(){

        $data=Survey::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Survey'
        ];

        return view('backend.master.survey.index',$params);

    }

    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Survey::find($id);
        }else{
            $data = new Survey();
        }
        $params = [
            'title' => 'Manajemen Survey',
            'data' => $data
        ];
        return view('backend.master.survey.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Survey::find($id);
            $data->verifikasi = $data->verifikasi;
        
        }else{
            $data = new Survey();
            $data->verifikasi='0';
        
        }

        $data->nama = $request->nama;
        $data->nik = $request->nik;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->jenis_kelamin = $request->jenis_kelamin;
        $data->pendidikan = $request->pendidikan;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Peran berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            dd($ex);
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            Survey::find($id)->delete();
            return "
            <div class='alert alert-success'>Peran berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal dihapus!</div>";
        }

    }

    public  function  update(Request $request){

        $id = intval($request->input('id', 0));

        try{
            Survey::where('id', $id)->update(['verifikasi' => '1']);
            return "
            <div class='alert alert-success'>Peran berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal dihapus!</div>";
        }

    }

}