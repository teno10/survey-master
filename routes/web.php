<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\BackendController@index')->middleware('verify-login');
Route::get('login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@login');
Route::group(['prefix' => 'backend', 'namespace' => 'Backend','middleware' => 'verify-login'], function () {
    Route::get('/', 'BackendController@index');
    Route::group(['prefix' => 'master', 'namespace' => 'Master','middleware' => 'verify-login'],function (){


        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'RoleController@index');
            Route::post('/add', 'RoleController@form');
            Route::post('/save', 'RoleController@save');
            Route::post('/delete', 'RoleController@delete');
        });
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'UserController@index');
            Route::post('/add', 'UserController@form');
            Route::post('/save', 'UserController@save');
            Route::post('/delete', 'UserController@delete');
        });

        Route::group(['prefix' => 'survey'], function () {
            Route::get('/', 'SurveyController@index');
            Route::post('/add', 'SurveyController@form');
            Route::post('/save', 'SurveyController@save');
            Route::post('/delete', 'SurveyController@delete');
            Route::post('/update', 'SurveyController@update');
        });

    });

});