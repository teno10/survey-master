<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nama" class="form-control" value="{{$data->nama}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>NIK</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nik" class="form-control" value="{{$data->nik}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Tanggal Lahir</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="date" name="tanggal_lahir" class="form-control daterange-cus" value="{{$data->tanggal_lahir}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Jenis Kelamin</label>
        <select name="jenis_kelamin" class="form-control">
            <option <?php if($data->jenis_kelamin == 'Laki-Laki') {echo "selected";}?> value="Laki-Laki">Laki-Laki</option>
            <option <?php if($data->jenis_kelamin == 'Perempuan') {echo "selected";}?> value="Perempuan">Perempuan</option>
        </select>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Pendidikan Terakhir</label>
        <select name="pendidikan" class="form-control">
            <option <?php if($data->pendidikan =='SD') {echo "selected";}?> value="SD">SD</option>
            <option <?php if($data->pendidikan =='SMP') {echo "selected";}?> value="SMP">SMP</option>
            <option <?php if($data->pendidikan =='SMA') {echo "selected";}?> value="SMA">SMA</option>
            <option <?php if($data->pendidikan =='Diploma') {echo "selected";}?> value="Diploma">Diploma</option>
            <option <?php if($data->pendidikan =='Sarjana') {echo "selected";}?> value="Sarjana">Sarjana</option>
        </select>
    </div>

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/survey/save', data, '#result-form-konten');
        })
    })
</script>