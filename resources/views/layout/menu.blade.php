<li>
    <a href="{{url('/backend')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
</li>
<li>
    <a href="#"><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
    @if(strtoupper(Session::get('activeUser')->getRole->nama_role)=='ADMINISTRATOR')
        <li><a href="{{url('/backend/master/users')}}">Pengguna</a></li>
        <li><a href="{{url('/backend/master/role')}}">Peran</a></li>
        <li><a href="{{url('/backend/master/survey')}}">Survey</a></li>
    @endif
    @if(strtoupper(Session::get('activeUser')->getRole->nama_role)=='SURVEYOR')
        <li><a href="{{url('/backend/master/survey')}}">Survey</a></li>
    @endif
    </ul>
</li>


<li>
    <a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
</li>